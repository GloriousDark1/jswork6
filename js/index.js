"use strict"

function createNewUser() {
    this.firstName = prompt("Назвіть своє ім'я ?");
    while (this.firstName === "") {
        this.firstName = prompt("Назвіть своє ім'я ?");
    }
    this.lastName = prompt("Яке ваше прізвище ?");
    while (this.lastName === "") {
        this.lastName = prompt("Яке ваше прізвище ?");
    }
    this.birthday = prompt("Вкажіть дату вашого народження в форматі (dd.mm.yyyy)", "dd.mm.yyyy");
    while (this.birthday === "") {
        this.birthday = prompt("Вкажіть дату вашого народження в форматі (dd.mm.yyyy)", "dd.mm.yyyy");
    }
    this.getLogin = function () {
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }
    this.getPassword = function () {
        let newPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        return newPassword;
    }
    this.getAge = function () {
        let reversDate = this.birthday.split('.').reverse().join('.');
        return +((new Date().getTime() - new Date(reversDate)) / (24 * 3600 * 365.25 * 1000)) | 0;
    }
}

let newObj = new createNewUser();
console.log(`${newObj.getPassword()}`);
console.log(`${newObj.getAge()}`);
console.log(newObj);
